import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Detalle from './components/Detalle';
import Home from './components/Home';
import RepoList from './components/RepoList';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
    },

    Details: {
      screen: Detalle,
    },

    RepoList: {
      screen: RepoList,
    },
  },
  {
    initialRouteName: 'Home',
  },
);

export default createAppContainer(AppNavigator);
