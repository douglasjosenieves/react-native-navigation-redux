export const set_text = data => {
	return {
		type: 'SET_TEXT',
		payload: {
			data,
		},
	};
};

