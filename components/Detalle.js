import React from 'react';
import {View, Text, Button} from 'react-native';
import {createAppContainer} from 'react-navigation';
import * as action from '../actions/actions';
import {createStackNavigator} from 'react-navigation-stack';
import {createStore} from 'redux';
import dummyReducer from '../reducers/reducer2';

import {connect} from 'react-redux';

class Detalle extends React.Component {
	render() {
		console.log(this.props);
		const {text} = this.props.reducer2;
		return (
			<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
				<Text>Details Screen</Text>
				<Text>{text}</Text>

				<Button
					title="Go to Home"
					onPress={() => {
						this.props.set_text('Francys');
						this.props.navigation.navigate('Home');
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => {
	let data = state.reducer2;
	return {
		reducer2: data,
	};
};

export default connect(
	mapStateToProps,
	action,
)(Detalle);
