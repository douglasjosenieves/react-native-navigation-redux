'use strict';

import React from 'react';

import {View, Text, Button} from 'react-native';

import {createAppContainer} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';
import {createStore} from 'redux';
import dummyReducer from '../reducers/reducer2';
import * as action from '../actions/actions';
import {connect} from 'react-redux';
import Detalle from './Detalle';

class Home extends React.Component {
	render() {
		console.log(this.props);
		const {text} = this.props.reducer2;
		return (
			<View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
				<Text>Home </Text>
				<Text>{text}</Text>

				<Button
					title="Go to Details"
					onPress={() => {
						// store.dispatch(SET_TEXT('John Doe'));
						this.props.set_text('Douglas');

						this.props.navigation.navigate('Details');
					}}
				/>

				<Button
					title="Go to Lista"
					onPress={() => {
						// store.dispatch(SET_TEXT('John Doe'));

						this.props.navigation.navigate('RepoList');
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => {
	let data = state.reducer2;
	return {
		reducer2: data,
	};
};

export default connect(
	mapStateToProps,
	action,
)(Home);
