import React, {Component} from 'react';
import {View, Text, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';

import * as action from '../reducers/reducer';

class RepoList extends Component {
  componentDidMount() {
    this.props.listRepos2('relferreira');
  }
  renderItem = ({item}) => (
    <TouchableOpacity onPress={() => this.actionOnRow(item)}>
      <View style={styles.item}>
        <Text>{item.name}</Text>
        <Text>{this.props.reducer2.text}</Text>
      </View>
    </TouchableOpacity>
  );

  actionOnRow(item) {
    console.log('Selected Item :', item);
  }

  render() {
    const {repos, reducer2} = this.props;

    console.log(reducer2);
    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        styles={styles.container}
        data={repos}
        renderItem={this.renderItem}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
});

const mapStateToProps = state => {
  let storedRepositories = state.reducer.repos.map(repo => ({
    key: repo.id,
    ...repo,
  }));
  let data = state.reducer2;
  return {
    repos: storedRepositories,
    reducer2: data,
  };
};

export default connect(
  mapStateToProps,
  action,
)(RepoList);
